"""gunicorn WSGI server configuration."""
from multiprocessing import cpu_count
from os import environ


def max_workers():
    return cpu_count()

bind = "unix:/src/squadEsp.sock"
workers = max_workers()
reload = True
