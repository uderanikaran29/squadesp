from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User
from accounts.models import create_player
import datetime
from game.models import GameTasks
# Create your views here.


def index(request):
    if request.user.is_authenticated:
        return redirect("/accounts/login/")
    else:
        return redirect("/game/tasks/")


def initialize_superuser(request):
    try:
        user = User.objects.create_user(username='squadadmin',
                                 email='admin@squadrun.com',
                                 password='admin12#')
        user.save()
    except:
        pass
        superuser = User.objects.get(username = 'squadadmin')
        superuser.is_superuser = True
        superuser.save()
    try:
        create_player(superuser)
    except:
        pass
    return HttpResponse("Super user successfully created")

@user_passes_test(lambda u: u.is_superuser)
def initialize_users(request):
    no_of_users = 10
    while no_of_users > 0:
        try:
            user = User.objects.create_user(username='user'+str(no_of_users),
                                 email='user'+str(no_of_users)+'@squadrun.co',
                                 password='password'+str(no_of_users)   )
            no_of_users = no_of_users - 1
            user.save()
            create_player(user)
        except Exception as e:
            return HttpResponse("Exception: "+ str(e))
    return HttpResponse("Users created")

@user_passes_test(lambda u: u.is_superuser)
def initialize_tasks(request):
    task_no = 1
    while task_no < 6:
        questions = []
        q_no = 1
        while q_no < 6:
            question = {}
            question['name'] = 'Question ' + str(q_no)
            question['primary'] = 'question' + str(q_no)
            secondary = 1
            secondary_images = []
            while secondary < 6:
                secondary_images.append('secondary'+str(secondary))
                secondary = secondary + 1
            question['secondary'] = secondary_images
            questions.append(question)
            q_no = q_no + 1
        task = GameTasks(
                            name = 'Task' + str(task_no),
                            questions = questions
                        )
        task.save()
        task_no = task_no + 1
    return HttpResponse("Tasks created")
