from accounts.models import Player
from django.db import models
from django.utils.crypto import get_random_string
from django.db.models import Q
from django.contrib.postgres.fields import JSONField

def make_uid():
    uid = get_random_string(length=8)
    try:
        device = GameTasks.objects.get(task_id = uid)
        if device:
            uids = make_uid()
            return uids
    except:
        return uid

# Create your models here.
class GameTasks(models.Model):
    task_id = models.CharField(max_length=8, primary_key = True, default= make_uid)
    name = models.CharField(max_length=100)
    questions = JSONField(null=True)

class TasksPlot(models.Model):
    task = models.ForeignKey(GameTasks, on_delete=models.CASCADE)
    player_1 = models.ForeignKey(Player, on_delete=models.CASCADE, related_name = "player_1")
    player_1_answers = JSONField(null=True)
    player_1_finished = models.BooleanField(default= False)
    paired = models.BooleanField(default=False)
    player_2 = models.ForeignKey(Player, on_delete=models.CASCADE, null = True, related_name = "player_2")
    player_2_answers = JSONField(null=True)
    player_2_finished = models.BooleanField(default= False)
    points = models.IntegerField(default = 0)

class TasksPlayed(models.Model):
    task = models.ForeignKey(GameTasks, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)


def get_game_tasks_left(player_tasks):
    tasks_to_exclude = player_tasks.values_list('task')
    tasks = GameTasks.objects.exclude(task_id__in=tasks_to_exclude)
    return tasks

def get_players_tasks(player):
    tasks = TasksPlayed.objects.filter(player=player)
    return tasks


def get_task_by_no(task_no):
    try:
        task = GameTasks.objects.get(task_id = task_no)
        return task
    except:
        return None

def generate_new_game(task, player):
    '''Try getting an existing game '''
    try:
        current_game = get_current_game(task, player)
        if current_game is not None:
            return current_game
    except:
        pass
    try:
        current_game = TasksPlot.objects.get(task = task, paired = False)
        current_game.paired = True
        current_game.player_2 = player
        current_game.save()
    except:
        current_game = TasksPlot(
                                    task = task,
                                    player_1 = player,
                                    player_1_answers = [None]*5,
                                    player_2_answers = [None]*5
                                )
        current_game.save()
    return current_game

def get_current_game(task, player):
    try:
        current_game = TasksPlot.objects.get( Q(task=task, player_1 = player ) | Q(task=task, player_2 = player))
        return current_game
    except:
        return None

def update_tasks_played(task, player):
    tasks_played = TasksPlayed(
                                task = task,
                                player = player
                                )
    tasks_played.save()
    return
