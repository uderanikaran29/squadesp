from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'tasks/', views.tasks, name='tasks'),
    url(r'play/(?P<task_no>[\w .@+-]+)',views.play, name='play'),
]
