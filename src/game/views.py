from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from accounts.models import get_player_by_user, get_player
from game.models import *


# Create your views here.
@login_required
def index(request):
    return redirect("/game/tasks/")


@login_required
def tasks(request):
    player = get_player_by_user(request.user)
    player_tasks = get_players_tasks(player)
    game_tasks_left = get_game_tasks_left( player_tasks )
    return render(request, 'game/tasks.html', { 'player': player,
                                                'player_tasks':player_tasks,
                                                'game_tasks_left': game_tasks_left
                                                } )

@login_required
def play(request, task_no):
    player = get_player_by_user(request.user)
    task = get_task_by_no(task_no)
    if request.method == 'GET':
        '''This means user has just started the game '''
        if task is None:
            return HttpResponse("No such task exist")
        '''Custom can be present here to check if has necessary requirements to unlock this task '''

        ''' Initialzing task from question 1 '''
        q_no = 0
        ''' Function called to generate new game or pair it with
         a user if a user has played this task and is not paired with anyone'''
        current_game = generate_new_game(task,player)
        if ( current_game.player_1_finished is True and current_game.player_1 == player ) or ( current_game.player_2_finished is True and current_game.player_2 == player ):
            messages.add_message(request, messages.SUCCESS, 'The game has already been completed. Score will be updated when the second player also completes the game')
            return redirect("/game/tasks/")
        question = current_game.task.questions[q_no]
    else:
        '''Check which question they are on currenlty'''
        q_no = int(request.POST.get("q_no"))
        current_game = get_current_game(task, player)
        if current_game is None:
            return HttpResponse("No such task exists.")
        record_answer(request, player, current_game)
        if q_no == 4:
            '''All tasks are completed'''
            task = get_task_by_no(task_no)
            update_tasks_played(task, player)
            if current_game.player_1 == player:
                current_game.player_1_finished = True
                messages.add_message(request, messages.SUCCESS, 'The score will be updated when the second player also completes the game')
            else:
                current_game.player_2_finished = True
                points = compute_points(current_game)
                messages.add_message(request, messages.SUCCESS, 'Great, you and your paired user scored '+str(points)  +' points !')
            current_game.save()
            return redirect("/game/tasks/")
        else:
            q_no = q_no + 1
            question = current_game.task.questions[q_no]
    return render(request, 'game/play.html', {
                                                'task':task,
                                                'question': question,
                                                'q_no': q_no,
                                                'player':player
                                            } )



'''Helper functions '''


def record_answer(request, player, current_game):
    q_no = int(request.POST.get("q_no"))
    answer = request.POST.get("answer")
    if player == current_game.player_1:
        current_game.player_1_answers[q_no] = answer
    else:
        current_game.player_2_answers[q_no] = answer
    current_game.save()
    return


def compute_points(current_game):
    q_no = 0
    points = 0
    while q_no < 5:
        if current_game.player_1_answers[q_no] == current_game.player_2_answers[q_no]:
            points = points + 1
        q_no = q_no + 1
    '''save point in game'''
    current_game.points = points

    '''save to player's profile '''
    print("player1"+current_game.player_2.player_id)
    print("player2"+current_game.player_2.player_id)
    player_1 = get_player(current_game.player_1.player_id)
    player_2 = get_player(current_game.player_2.player_id)
    player_1.points = player_1.points + points
    player_2.points = player_2.points + points

    '''commit to database'''
    player_1.save()
    player_2.save()
    current_game.save()

    return points
