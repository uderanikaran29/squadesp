from django.contrib.auth.models import User
from django.db import models
from django.utils.crypto import get_random_string

# Create your models here.
def make_uid():
    uid = get_random_string(length=8)
    try:
        device = GameTasks.objects.get(task_id = uid)
        if device:
            uids = make_uid()
            return uids
    except:
        return uid


class Player(models.Model):
    player_id = models.CharField(max_length=8, primary_key = True, default= make_uid)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    last_played = models.DateTimeField(null = True)
    points = models.IntegerField(default =0)


def get_player_by_user(user):
    try:
        return Player.objects.get(user_id = user)
    except:
        return None

def get_player(player_id):
    try:
        return Player.objects.get(player_id = player_id)
    except:
        return None





def create_player(user):
    player = Player(
                        user_id = user
                    )
    player.save()
    return
