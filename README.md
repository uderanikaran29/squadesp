# README #

The assignment is live on https://104.211.189.140/

### Features implemented ###

* The assignment has following features
* Session based auth
* Dashboard to show messages, task played and overall profile points
* Tasks are paired whenever someone plays the same tasks 
* Points are updated when one more person plays that task

* When logged out https://104.211.189.140/accounts/login/

### How do I get set up? ###

* Install docker from https://docs.docker.com/engine/installation/
* Run simple command of docker-compose up (use sudo if you do not have permissions , with -d to run in background ) 
* Once the site is running on your local machine enter following urls to setup
* Create a superuser: https://104.211.189.140/initialize_superuser/ username: squadadmin password : admin12#
* Create Users: https://104.211.189.140/initialize_users/ username: user1 password: password1 (where number can range from 1-10)
   ie. user10, password10
* Create Tasks: https://104.211.189.140/initialize_tasks/


### Verification ? ###
* Once User Logins two same tasks from different accounts, the points will be updated and reflected in theri accounts profile

### Screenshots ###

![picture](screenshots/1.png)
![picture](screenshots/2.png)
![picture](screenshots/3.png)

### Backend Architecture ####
VM : Docker
Server: Nginx
WSGI: Gunicorn
Web Framework: Django
Databases: Postgres
